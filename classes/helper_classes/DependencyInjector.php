<?php
//this Class is used to inject dependencies 
//set method takes the key and value, it stores it in an array and get method returns the value of the key passed (that is dependency that is required) if it is stored/set in an array else it dies with an message
class DependencyInjector
{
    private $dependencies = [];

    public function set(string $key, $value){
        $this->dependencies[$key] = $value;
    }

    public function get(string $key){
        if(isset($this->dependencies[$key])){
            return $this->dependencies[$key];
        }
        die('This dependency Not Found : ' . $key);
    }
}
?>