<?php
class Validator{
    private $di;
    private $id;
    protected $rules = ['required', 'minlength', 'maxlength', 'unique', 'email', 'phone', 'unique_not_me', 'exists'];

    protected $messages = [
        'required' => 'The :field field is required',
        'minlength' => 'The :field field must be a minimum of :satisfier characters',
        'maxlength' => 'The :field field must be a maximum of :satisfier characters',
        'email' => 'This is not a valid email address',
        'unique' => 'That :field is already taken',
        'phone' => 'That is not a valid phone number',
        'unique_not_me' => 'That :field is already taken',
        'exists' => 'The :field is not valid'
    ];

    public function __construct($di){
        $this->di = $di;
    }

    public function check($items, $rules, $id=""){
        $this->id = $id;

        foreach($items as $item => $value){
            if(is_array($value)){
                foreach($value as $singleVal){
                    if(in_array($item, array_keys($rules))){
                        $this->validate([
                            'field' => $item,
                            'value' => $singleVal,
                            'rules' => $rules[$item]
                        ], $id);
                    }
                }
            }else{
                if(in_array($item, array_keys($rules))){
                    $this->validate([
                        'field' => $item,
                        'value' => $value,
                        'rules' => $rules[$item]
                    ]);
                }
            }
        }
        return $this;
    }

    public function validate($item){
        $field = $item['field'];
        foreach($item['rules'] as $rule => $satisfier){
            if(!call_user_func_array([$this, $rule], [$field, $item['value'], $satisfier])){
                //error Handling
                $this->di->get('errorhandler')->addError(str_replace([':field', ':satisfier'], [$field, $satisfier], $this->messages[$rule]), $field);
            }
        }
    }

    public function required($field, $value, $satisfier){
        return !empty(trim($value));
    }
    public function minlength($field, $value, $satisfier){
        return mb_strlen($value) >= $satisfier;
    }
    public function maxlength($field, $value, $satisfier){
        return mb_strlen($value) <= $satisfier;
    }
    public function unique($field, $value, $satisfier){
        return !$this->di->get('database')->exists($satisfier, [$field=>$value]);
    }
    public function unique_not_me($field, $value, $satisfier){
        $query = "SELECT COUNT(*) as count FROM {$satisfier} WHERE {$field} = '{$value}' AND id != {$this->id}";
        
        return !$this->di->get('database')->raw($query)[0]->count;
    }
    public function email($field, $value, $satisfier){
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }
    public function phone($field, $value, $satisfier){
        return strlen(preg_replace('/^[0-9]{10}/', '', $value)) == 10;
    }
    public function fails(){
        return $this->di->get('errorhandler')->hasErrors();
    }
    public function exists($field, $value, $satisfier){
        $link = explode("|", $satisfier);
        $data= [
            $link[1]=> $value
        ];
        return ($this->di->get('database')->exists($link[0], $data));
    }

    public function errors(){
        return $this->di->get('errorhandler');
    }
}
?>