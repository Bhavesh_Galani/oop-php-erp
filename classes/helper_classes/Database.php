<?php
class Database
{
    private $di;
    private $pdo;
    private $stmt;

    private $debug;
    private $config;
    private $host;
    private $username;
    private $password;
    private $db;

    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->config = $this->di->get('config');
        $this->debug = $this->config->get('debug');
        $this->host = $this->config->get('host');
        $this->username = $this->config->get('username');
        $this->password = $this->config->get('password');
        $this->db = $this->config->get('db');
        $this->connectDB();
    }
    private function connectDB()
    {
        try{
            // echo "Connecting to DB";
            $this->pdo = new PDO("mysql:host={$this->host};dbname={$this->db}", $this->username, $this->password);
            if($this->debug){
                $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
        }catch (PDOException $e) {
            die($this->debug ? $e->getMessage() : 'Error While performing something!');
        }
    }

    public function raw(string $sql, $mode = PDO::FETCH_OBJ)
    {
        return $this->pdo->query($sql)->fetchAll($mode);
    }

    public function query(string $sql)
    {
        return $this->pdo->query($sql);
    }

    /**
     * Function inserts $data in $table and returns the insert id of the record.
     * The insert id is actually returned so that it can be used for the linking tables.
     */
    public function insert(string $table, $data)
    {
        $keys = array_keys($data);
        $fields = "`" . implode("`, `", $keys). "`";

        $placeholders = ":". implode(", :", $keys);

        $sql = "INSERT INTO {$table} ({$fields}) VALUES ({$placeholders})";
        $this->stmt = $this->pdo->prepare($sql);

        $this->stmt->execute($data);
        return $this->pdo->lastInsertId();
    }
    public function multiInsert(string $table, $data){
        $fields = implode(", ", array_keys($data[0]));

        $valuesArray = array();
        
        $placeholders = "";
        $dataString = " (";
        $noOfColumns = sizeof($data[0]);
        for($i=0; $i<$noOfColumns; $i++) {
            if($i==$noOfColumns-1){
                $dataString .= "?)";
            }else{
                $dataString .= "?, ";
            }
        }
        
        
        foreach ($data as $row) {
            $placeholders .= $dataString.",";
            foreach ($row as $value) {
                array_push($valuesArray, $value);
            }
        }
        $placeholders = substr($placeholders, 0, -1);
        $sql = "INSERT INTO {$table} ($fields) VALUES{$placeholders}";
        $this->stmt = $this->pdo->prepare($sql);
        $this->stmt->execute($valuesArray);
        return $this->pdo->lastInsertId();
     }

    public function lastInserId()
    {
        return $this->pdo->lastInsertId();
    }

    public function readData($table, $fields = [], $condition = "1", $readMode = PDO::FETCH_OBJ)
    {
        if(count($fields) == 0)
        {
            $columnNameString = "*";
        }else{
            $columnNameString = implode(", ", $fields);
        }
        $sql = "SELECT {$columnNameString} FROM {$table} WHERE {$condition} AND deleted=0";
        $this->stmt = $this->pdo->prepare($sql);
        // Util::dd($sql);
        $this->stmt->execute();
        return $this->stmt->fetchAll($readMode);
    }

    

    public function delete($table, $condition)
    {
        $sql = "UPDATE {$table} SET deleted = 1 WHERE {$condition}";
        $this->stmt = $this->pdo->prepare($sql);
        $this->stmt->execute();
    }
    public function hardDelete($table, $condition){
        $sql = "DELETE FROM {$table} WHERE {$condition}";
        $this->query($sql);
    }

    public function update($table, $data, $condition="1")
    {
        // die($data);
        $columnKeyValue = "";
        $i = 0;
        $newData = [];
        foreach($data as $key=>$value){
            $keyForPlaceholders = str_replace(".", "0", $key);
            // echo($keyForPlaceholders);
            // echo "<br>";
            $columnKeyValue .= "$key = :$keyForPlaceholders";
            // echo($columnKeyValue);
            // echo "<br>";
            // Masti
            $newData[$keyForPlaceholders] = $value;

            $i++;
            if($i<count($data)){
                $columnKeyValue .= ",";
            }
        }
        // $this->stmt = $this->pdo->query("UPDATE customers SET customers.first_name='aaaa' WHERE id=10");
        $sql = "UPDATE {$table} SET {$columnKeyValue} WHERE {$condition}";
        $this->stmt = $this->pdo->prepare($sql);
        // // Regex accepted by sql in placeholders!   // [:][a-zA-Z0-9_]+;
        // // Util::dd($sql);
        // // Util::dd($newData);
        // // Util::dd($this->stmt->execute($newData));
        $this->stmt->execute($newData);
        return $this;
    }

    public function exists($table, $data)
    {
        $field = array_keys($data)[0];

        $result= $this->readData($table, [], "{$field} = '{$data[$field]}'", PDO::FETCH_ASSOC);
        if(count($result)>0){
            return true;
        }else{
            return false;
        }
    }

    public function beginTransaction()
    {
        return $this->pdo->beginTransaction();
    }

    public function commit()
    {
        return $this->pdo->commit();
    }

    public function rollback()
    {
        return $this->pdo->rollback();
    }
}