<?php
require_once __DIR__."/../helper/requirements.php";

class Sales{
    private $table = "sales";
    private $database;
    protected $di;
    
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    private function validateData($data, $id=""){
        $validator = $this->di->get('validator');
        $rules = [
            'product_id'=>[
                'exists'=>'products|id'
            ]
        ];
        return ($id == "") ? $validator->check($data, $rules): $validator->check($data, $rules, $id);
    }

    public function create($data){
        $validation = $this->validateData($data);
        if(!$validation->fails())
        {
            try{
                $customer_id = $data['customer_id'];
                // Util::dd($data);
                $this->database->beginTransaction();
                $inv_id = $this->database->insert("invoice", ["customer_id"=>$customer_id]);
                $data_to_be_inserted = array();
                for($i=0; $i<sizeof($data['product_id']); $i++){
                    // Util::dd($data['product_id'][0]);
                    array_push($data_to_be_inserted, ["product_id"=>$data['product_id'][$i], "quantity"=>$data["quantity"][$i], "discount"=>$data["quantity"][$i], "invoice_id"=>$inv_id]);
                }
                $this->database->multiInsert("sales", $data_to_be_inserted);
                $this->database->commit();
                $_SESSION['inv_id'] = $inv_id;
                return ADD_SUCCESS;
            }catch(Exception $e)
            {
                $this->database->rollback();
                return ADD_ERROR;
            }
        }else{
            return VALIDATION_ERROR;
        }
    }
}
?>