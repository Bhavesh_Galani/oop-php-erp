<?php
require_once __DIR__."/../helper/requirements.php";

class Product{
    private $table = "products";
    private $database;
    protected $di;
    
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    // If id is passed it will work as edit else as it's new it will work as add!!!
    private function validateData($data, $id=""){
        $validator = $this->di->get('validator');
        $rules = [
            
            'specification' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ],
            'category_id' => [
                'required' => true
            ],
            'hsn_code' => [
                'required' => true,
                'minlength' => 8,
                'maxlength' => 8
                
            ],
            'supplier_id' => [
                'required' => true,
                'exists' => 'suppliers|id'
            ],
            'eoq_level' => [
                'required' => true,
                'minlength' => 1,
                'maxlength' => 5
            ],
            'danger_level' => [
                'required' => true,
                'minlength' => 1,
                'maxlength' => 5
            ],
            'quantity' => [
                'required' => true,
                'minlength' => 1,
                'maxlength' => 11
            ],
            'selling_rate' => [
                'required' => true,
                'minlength' => 1
            ]
        ];
        if($id == null){
            $rules['name'] = [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255,
                'unique' => $this->table
            ];
        }else{
            $rules['name'] = [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255,
                'unique_not_me' => $this->table
            ];
        }
        return ($id == "") ? $validator->check($data, $rules): $validator->check($data, $rules, $id);
    }

    public function addProduct($data)
    {
        $validation = $this->validateData($data);
        if(!$validation->fails())
        {
            //Validation was successful
            // Util::dd("Success!");
            try
            {
                $columnsOfProductTable = ["name", "specification", "hsn_code", "category_id", "eoq_level", "danger_level", "quantity"];
                $data_to_be_inserted = Util::createAssocArray($columnsOfProductTable, $data);
                //Begin Transaction
                $this->database->beginTransaction();

                $product_id = $this->database->insert($this->table, $data_to_be_inserted);

                $data_to_be_inserted = [];
                $data_to_be_inserted['product_id'] = $product_id;
                foreach($data['supplier_id'] as $supplier_id){
                    $data_to_be_inserted['supplier_id'] = $supplier_id;
                    $this->database->insert('product_supplier', $data_to_be_inserted);
                }

                $data_to_be_inserted = [];
                $data_to_be_inserted['product_id'] = $product_id;
                $data_to_be_inserted['selling_rate'] = $data['selling_rate'];

                $this->database->insert('products_selling_rate', $data_to_be_inserted);
                $this->database->commit();
                return ADD_SUCCESS;
            }
            catch(Exception $e)
            {
                $this->database->rollback();
                return ADD_ERROR;
            }
        }
        else
        {
            return VALIDATION_ERROR;
        }
    }
    public function getJSONDataForDataTable($draw,$searchParameter,$orderBy,$start,$length)
    {
        // not working!
        // SELECT products.id, products.name as product_name, products.specification, products.eoq_level, products.danger_level, categories.name as category_name, products_selling_rate.selling_rate,MAX(products_selling_rate.with_effect_from), GROUP_CONCAT(CONCAT(first_name, ' ', last_name)) as supplier_name FROM products INNER JOIN categories ON products.category_id = categories.id INNER JOIN product_supplier ON products.id = product_supplier.product_id INNER JOIN suppliers ON product_supplier.supplier_id = suppliers.id INNER JOIN products_selling_rate ON products.id = products_selling_rate.product_id GROUP BY products.id

        $columns = ["products.name", "products.specification", "products_selling_rate.selling_rate", "products_selling_rate.with_effect_from", "products.eoq_level", "products.danger_level", "category_name", "supplier_name"];

        $query = "SELECT products.id, products.name as product_name, products.specification, products.eoq_level, products.danger_level, category.name as category_name, products_selling_rate.selling_rate, products_selling_rate.with_effect_from, GROUP_CONCAT(CONCAT(first_name, ' ', last_name)) as supplier_name FROM products INNER JOIN category ON products.category_id = category.id INNER JOIN product_supplier ON products.id = product_supplier.product_id INNER JOIN suppliers ON product_supplier.supplier_id = suppliers.id INNER JOIN products_selling_rate ON products.id = products_selling_rate.product_id INNER JOIN (SELECT product_id, MAX(with_effect_from) as wef FROM (SELECT * FROM `products_selling_rate` WHERE with_effect_from <= CURRENT_TIMESTAMP) as temp GROUP BY product_id) as max_date_table ON max_date_table.product_id = products_selling_rate.product_id AND products_selling_rate.with_effect_from = max_date_table.wef WHERE products.deleted = 0";
        $groupBy = " GROUP BY products.id";

        $totalRowCountQuery = "SELECT DISTINCT(count(*) OVER()) as total_count FROM products INNER JOIN category ON products.category_id = category.id INNER JOIN product_supplier ON products.id = product_supplier.product_id INNER JOIN suppliers ON product_supplier.supplier_id = suppliers.id INNER JOIN products_selling_rate ON products.id = products_selling_rate.product_id INNER JOIN (SELECT product_id, MAX(with_effect_from) as wef FROM (SELECT * FROM `products_selling_rate` WHERE with_effect_from <= CURRENT_TIMESTAMP) as temp GROUP BY product_id) as max_date_table ON max_date_table.product_id = products_selling_rate.product_id AND products_selling_rate.with_effect_from = max_date_table.wef WHERE products.deleted = 0";

        $filterRowCountQuery = $totalRowCountQuery;

        if($searchParameter != null)
        {
            $condition = " AND products.name like '%{$searchParameter}%' OR products.specification like '%{$searchParameter}%' OR category.name like '%{$searchParameter}%' OR suppliers.first_name like '%{$searchParameter}%' OR suppliers.last_name like '%{$searchParameter}%'";

            $query .= $condition;
            $filterRowCountQuery .= $condition;
        }

        $query .= $groupBy;
        $filterRowCountQuery .= $groupBy;
        $totalRowCountQuery .= $groupBy;

        if($orderBy != null)
        {
            $query .= " ORDER BY {$columns[$orderBy[0]['column']]} {$orderBy[0]['dir']}";
        }
        else
        {
            // ;
        }

        if($length != -1)
        {
            $query .=  " LIMIT {$start},{$length}";
        }

        $totalRowCountResult = $this->database->raw($totalRowCountQuery);
        $numberOfTotalRows = is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count : 0;
    
        $filteredRowCountResult = $this->database->raw($filterRowCountQuery);
        $numberOfFilteredRows = is_array($filteredRowCountResult) ? ($filteredRowCountResult[0]->total_count ?? 0) : 0;

        $filteredData = $this->database->raw($query);
        $numberOfRowsToDisplay = is_array($filteredData) ? count($filteredData):0;

        $data = [];
        for($i=0;$i<$numberOfRowsToDisplay;$i++)
        {
            $subarray = [];
            $subarray[] = $filteredData[$i]->product_name;
            $subarray[] = $filteredData[$i]->specification;
            $subarray[] = $filteredData[$i]->selling_rate;
            $subarray[] = $filteredData[$i]->with_effect_from;
            $subarray[] = $filteredData[$i]->eoq_level;
            $subarray[] = $filteredData[$i]->danger_level;
            $subarray[] = $filteredData[$i]->category_name;
            $subarray[] = $filteredData[$i]->supplier_name;

            $subarray [] = <<<BUTTONS
            <div class="d-flex">
                <button class='view btn btn-outline-warning d-block' id='{$filteredData[$i]->id}'
                data-toggle="modal" href=""><i class="fas fa-eye"></i></button>

                <button class='edit btn btn-outline-primary d-block ml-2' id='{$filteredData[$i]->id}'
                data-toggle="modal" data-target=""><i class="fas fa-pencil-alt"></i></button>

                <button class='delete btn btn-outline-danger d-block ml-2' id='{$filteredData[$i]->id}'
                data-toggle="modal" data-target="#deleteModal"><i class="fas fa-trash"></i></button>
            </div>
BUTTONS;
            $data[] = $subarray;
        }

        $output = array(
            "draw"=>$draw,
            "recordsTotal"=>$numberOfTotalRows,
            "recordsFiltered"=>$numberOfFilteredRows,
            "data"=>$data
        );

        echo json_encode($output);
    }
    public function getProductById($productId,$mode=PDO::FETCH_OBJ){
        $query = "SELECT products.id, products.name, products.specification, products.eoq_level, products.danger_level, products.hsn_code, products.quantity, category.id as category_id, products_selling_rate.selling_rate, products_selling_rate.with_effect_from, GROUP_CONCAT(suppliers.id) as supplier_id FROM products INNER JOIN category ON products.category_id = category.id INNER JOIN product_supplier ON products.id = product_supplier.product_id INNER JOIN suppliers ON product_supplier.supplier_id = suppliers.id INNER JOIN products_selling_rate ON products.id = products_selling_rate.product_id INNER JOIN (SELECT product_id, MAX(with_effect_from) as wef FROM (SELECT * FROM `products_selling_rate` WHERE with_effect_from <= CURRENT_TIMESTAMP) as temp GROUP BY product_id) as max_date_table ON max_date_table.product_id = products_selling_rate.product_id AND products_selling_rate.with_effect_from = max_date_table.wef WHERE products.deleted = 0 AND products.id = '$productId'";
        $result = $this->database->raw($query,$mode);
        return $result;
    }
    public function update($data, $id){
        $validation = $this->validateData($data, $id);
        if(!$validation->fails()){
            try
            {
                $columnsOfProductTable = ["name", "specification", "hsn_code", "category_id", "eoq_level", "danger_level", "quantity"];
                $data_to_be_updated = Util::createAssocArray($columnsOfProductTable, $data);
                // Suppliers calculation!
                $suppliers = $data['supplier_id'];
                $oldSuppliers = $this->database->raw("SELECT supplier_id FROM product_supplier WHERE product_id = $id", PDO::FETCH_ASSOC);
                $temp = [];
                foreach($oldSuppliers as $supplier){
                    $temp[] = $supplier["supplier_id"];
                }
                $oldSuppliers = $temp;


                $suppliersToBeRemoved = array_diff($oldSuppliers, $suppliers);
                $suppliersToBeAdded = array_diff($suppliers, $oldSuppliers);
                // Util::dd($suppliersToBeAdded);
                // Util::dd($suppliersToBeRemoved);

                /* Begin Transaction */
                $this->database->beginTransaction();

                //Updating product table
                $this->database->update($this->table, $data_to_be_updated, "id = {$id}");
                //Removing old suppliers(Hard Delete)
                if(!empty($suppliersToBeRemoved)){
                    $condition_to_delete = "product_id = {$id}";
                    $i=0;
                    foreach($suppliersToBeRemoved as $supplier_id){
                        if($i == 0){
                            $condition_to_delete .= " AND (supplier_id = {$supplier_id}";
                        }else{
                            $condition_to_delete .= " OR supplier_id = {$supplier_id}";
                        }
                        $i++;
                    }
                    $condition_to_delete .= ")";
                    $this->database->hardDelete("product_supplier", $condition_to_delete);
                }

                //Inserting new Suppliers
                if(!empty($suppliersToBeAdded)){
                    $data_to_be_inserted = [];
                    $data_to_be_inserted['product_id'] = $id;
                    foreach ($suppliersToBeAdded as $supplier_id){
                        $data_to_be_inserted['supplier_id'] = $supplier_id;
                        $this->database->insert('product_supplier', $data_to_be_inserted);
                    }
                }
                
                //Inserting new product selling_rate wef!
                if(!(($old_selling_rate = $this->database->raw("SELECT selling_rate FROM products_selling_rate WHERE product_id = $id", PDO::FETCH_ASSOC)[0]['selling_rate']) == $data['selling_rate'])){
                    $this->database->insert('products_selling_rate', ['product_id' => $id, 'selling_rate' => $data['selling_rate']]);
                }
                $this->database->commit();
                return EDIT_SUCCESS;
            }catch(Exception $e)
            {
                $this->database->rollback();
                return EDIT_ERROR;
            }
        }
        return VALIDATION_ERROR;
    }
    public function getProductsByCategoryID($category_id){
        return $this->database->readData('products', ['id', 'name'], "category_id = {$category_id} and deleted=0");
    }
    public function getSellingPrice($product_id){
    return $this->database->raw("SELECT selling_rate FROM products_selling_rate WHERE product_id = {$product_id} AND with_effect_from<= CURRENT_TIMESTAMP order by with_effect_from desc LIMIT 1")[0]->selling_rate;
    }
}
?>