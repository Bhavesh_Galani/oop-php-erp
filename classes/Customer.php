<?php

require_once __DIR__."/../helper/requirements.php";

class Customer{
    private $table1 = "customers";
    private $table2 = "address_customer";
    private $table3 = "address";
    private $database;
    protected $di;
    
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    
    private function validateData($data, $id="")
    {
        // Util::dd($data);
        $validator = $this->di->get('validator');
        $rules = [
            'first_name' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ],
            'gender' => [
                'required'=> true
            ],
            'last_name' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ],
            'phone_no' => [
                'required' => true,
                'minlength' => 10,
                'maxlength' => 10
            ],
            'country' => [
                'required' => true
            ],
            'block_no' => [
                'required' => true,
                'minlength' => 3
            ],
            'street' => [
                'required' => true,
                'minlength' => 3
            ],
            'city' => [
                'required' => true,
                'minlength' => 3
            ],
            'pincode' => [
                'required' => true,
                'minlength' => 6,
                'maxlength' => 6
            ],
            'state' => [
                'required' => true,
                'minlength' => 2
            ],
            'town' => [
                'required' => true,
                'minlength' => 3
            ]
        ];
        if(!empty($data['gst_no'])){
            $rules['gst_no'] = [
                'unique_not_me' => $this->table1,
                'minlength' => 15
            ];
        }
        if($id == ""){
            $rules['email_id'] = [
                'unique' => $this->table1,
                'email' => true
            ];
        }else{
            $rules['email_id'] = [
                'unique_not_me' => $this->table1,
                'email' => true
            ];
        }
        return ($id == "") ?  $validator->check($data, $rules): $validator->check($data, $rules, $id);
    }
    /**
     * This function is responsible to accept the data from the Routing and add it to the Database.
     */
    public function addCustomer($data)
    {
        $validation = $this->validateData($data);
        if(!$validation->fails())
        {
            //Validation was successful
            // Util::dd("Success!");
            try
            {
                //Begin Transaction
                $this->database->beginTransaction();
                $data_to_be_inserted = [
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'phone_no' => $data['phone_no'],
                    'email_id' => $data['email_id'],
                    'gst_no' => $data['gst_no'],
                    'gender' => $data['gender']
                ];
                // Util::dd($data_to_be_inserted);
                $customer_id = $this->database->insert($this->table1, $data_to_be_inserted);
                $this->database->commit();

                $data_to_be_inserted = [
                    'block_no' => $data['block_no'],
                    'street' => $data['street'],
                    'city' => $data['city'],
                    'pincode' => $data['pincode'],
                    'state' => $data['state'],
                    'country' => $data['country'],
                    'town' => $data['town']
                ];
                $address_id = $this->database->insert($this->table3, $data_to_be_inserted);
                $data_to_be_inserted = [
                    'address_id' => $address_id,
                    'customer_id' => $customer_id
                ];
                $address_customer_id = $this->database->insert($this->table2, $data_to_be_inserted);

                return ADD_SUCCESS;
            }
            catch(Exception $e)
            {
                $this->database->rollback();
                return ADD_ERROR;
            }
        }
        else
        {
            //Validation Failed!
            return VALIDATION_ERROR;
        }
    }

    public function getJSONDataForDataTable($draw,$searchParameter,$orderBy,$start,$length){
        // columns which can be $orderBy
        $columns = ["full_name", "phone_no", "email_id", "full_address"];

        
        $totalRowCountQuery = "SELECT COUNT(id) as total_count FROM ($this->table1) WHERE deleted = 0";

        $filteredRowCountQuery = "SELECT CONCAT(customers.first_name, ' ', customers.last_name) AS full_name, CONCAT(address.block_no, ',', address.street, ',', address.city, ',', address.town, ',', address.state, ',', address.country) AS full_address  FROM address, address_customer, customers WHERE address.id = address_customer.id AND address_customer.customer_id = customers.id AND customers.deleted = 0";
        
        // actual query for data
        $query = "SELECT customers.id, CONCAT(customers.first_name, ' ', customers.last_name) AS full_name, customers.phone_no, customers.email_id, CONCAT(address.block_no, ',', address.street, ',', address.city, ',', address.town, ',', address.state, ',', address.country) AS full_address FROM address, address_customer, customers WHERE address.id = address_customer.address_id AND address_customer.customer_id = customers.id AND customers.deleted = 0";

        if($searchParameter != null){
            // $temp = "HAVING full_name like '%{$searchParameter}%' OR full_address like '%{$searchParameter}%'";
            // $query .= $temp;
            // $filteredRowCountQuery .= $temp;
            $temp = " AND CONCAT(customers.first_name, ' ', customers.last_name) like '%{$searchParameter}%' OR CONCAT(address.block_no, ',', address.street, ',', address.city, ',', address.town, ',', address.state, ',', address.country) like '%{$searchParameter}%'";

            $query .= $temp;
            $filteredRowCountQuery .= $temp;
        }

        if($orderBy !=null)
        {
            $query .= " ORDER BY {$columns[$orderBy[0]['column']]} {$orderBy[0]['dir']}";
        }

        if($length !=-1)
        {
            $query .=  " LIMIT {$start},{$length}";
        }

        $totalRowCountResult = $this->database->raw($totalRowCountQuery);
        $numberOfTotalRows = is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count : 0;
    
        $filteredRowCountResult = $this->database->raw($filteredRowCountQuery);
        $numberOfFilteredRows = is_array($filteredRowCountResult) ? count($filteredRowCountResult) : 0;

        $filteredData = $this->database->raw($query);
        $numberOfRowsToDisplay = is_array($filteredData) ? count($filteredData):0;

        $data = [];
        for($i=0;$i<$numberOfRowsToDisplay;$i++)
        {
            $subarray = [];
            // name - FullName
            $subarray[] = $filteredData[$i]->full_name;

            // phone_no
            $subarray[] = $filteredData[$i]->phone_no;

            // email_id
            $subarray[] = $filteredData[$i]->email_id;

            // address
            $subarray[] = $filteredData[$i]->full_address;

            
            // actions
            $subarray [] = <<<BUTTONS
            <div class="d-flex">
                <button class='view btn btn-outline-warning d-block' id='{$filteredData[$i]->id}'
                data-toggle="modal" href=""><i class="fas fa-eye"></i></button>

                <button class='edit btn btn-outline-primary d-block ml-2' id='{$filteredData[$i]->id}'
                data-toggle="modal" data-target=""><i class="fas fa-pencil-alt"></i></button>

                <button class='delete btn btn-outline-danger d-block ml-2' id='{$filteredData[$i]->id}'
                data-toggle="modal" data-target="#deleteModal"><i class="fas fa-trash"></i></button>
            </div>
BUTTONS;
            $data[] = $subarray;
        }
        // Util::dd($data);

        $output = array(
            "draw"=>$draw,
            "recordsTotal"=>$numberOfTotalRows,
            "recordsFiltered"=>$numberOfFilteredRows,
            "data"=>$data
        );

        echo json_encode($output);
    }

    public function getCustomerById($customerId,$mode=PDO::FETCH_OBJ)
    {
        $query = "SELECT * FROM {$this->table1} WHERE deleted=0 AND id = {$customerId}";
        $result = $this->database->raw($query,$mode);
        return $result;
    }
    
    public function getAddressById($customerId, $mode=PDO::FETCH_OBJ){
        $query = "SELECT * FROM address WHERE address.id IN (SELECT customer_id from address_customer WHERE customer_id IN (SELECT id from customers WHERE id={$customerId}))";
        $result = $this->database->raw($query, $mode);
        return $result;
    }

    public function delete($id)
    {
         try {
            $this->database->beginTransaction();
            $sql = "UPDATE address, address_customer, customers SET customers.deleted=1, address.deleted=1 WHERE address.id = address_customer.address_id AND address_customer.customer_id = customers.id AND customers.id = {$id}";
            $this->database->query($sql);
            $this->database->commit();
            return DELETE_SUCCESS;
        } catch (Exception $e) {
             $this->database->rollback();
             return DELETE_ERROR;
        }
    }

    public function update($data, $id){
        // Util::dd($data);
        $validation = $this->validateData($data, $id);
        if(!$validation->fails()){
            try
            {
                //Begin Transaction
                $this->database->beginTransaction();
                // $data_to_be_inserted = [
                //     'first_name' => $data['first_name'],
                //     'last_name' => $data['last_name'],
                //     'phone_no' => $data['phone_no'],
                //     'email_id' => $data['email_id'],
                //     'gst_no' => $data['gst_no'],
                //     'gender' => $data['gender']
                // ];
                // Util::dd($data_to_be_inserted);
                // $customer_id = $this->database->update($this->table1, $data_to_be_inserted, "id"+$id);
                // $this->database->commit();

                // $data_to_be_inserted = [
                //     'block_no' => $data['block_no'],
                //     'street' => $data['street'],
                //     'city' => $data['city'],
                //     'pincode' => $data['pincode'],
                //     'state' => $data['state'],
                //     'country' => $data['country'],
                //     'town' => $data['town']
                // ];
                // $address_id = $this->database->update($this->table3, $data_to_be_inserted, "customer_id"+$customer_id);
                // $data_to_be_inserted = [
                //     'address_id' => $address_id,
                //     'customer_id' => $customer_id
                // ];
                // $address_customer_id = $this->database->insert($this->table2, $data_to_be_inserted, "address_id");
                    
                // $query = "UPDATE address INNER JOIN address_customer ON address.id = address_customer.address_id INNER JOIN customers ON address_customer.customer_id = customers.id AND customers.id = 10 SET address.city = "Chembur"";

                $data_to_be_inserted = [
                    'address.block_no' => $data['block_no'],
                    'address.street' => $data['street'],
                    'address.city' => $data['city'],
                    'address.pincode' => $data['pincode'],
                    'address.state' => $data['state'],
                    'address.country' => $data['country'],
                    'address.town' => $data['town'],
                    'customers.first_name' => $data['first_name'],
                    'customers.last_name' => $data['last_name'],
                    'customers.phone_no' => $data['phone_no'],
                    'customers.email_id' => $data['email_id'],
                    'customers.gst_no' => $data['gst_no'],
                    'customers.gender' => $data['gender']
                ];
                $this->database->update("address, address_customer, customers", $data_to_be_inserted, "address.id = address_customer.address_id AND address_customer.customer_id = customers.id AND customers.id = '".$id."'");
                // Util::dd("Success!");
                $this->database->commit();
                return EDIT_SUCCESS;
            }
            catch(Exception $e)
            {
                $this->database->rollback();
                return EDIT_ERROR;
            }
        }
        else
        {
            //Validation Failed!
            return VALIDATION_ERROR;
        }
    }
    public function getCustomerByEmail($email){
        if($this->database->exists("customers", ["email_id"=>$email])){
            return $this->database->raw("SELECT IFNULL(id, 0) as id FROM customers WHERE email_id = '$email'")[0]->id;
        }else{
            return "";
        }

    }
}
?>