<?php
require_once __DIR__ . '/../../helper/init.php';
// Util::dd($_GET['id']);
$id = Session::getSession('view-customer-id');
// Util::dd($id);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php require_once __DIR__ . "/../includes/head-section.php"; ?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Page Level CSS -->
    <link rel="stylesheet" href="<?=BASEASSETS?>css/view-customer.css">
    <title>Document</title>
</head>
<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php require_once(__DIR__ . "/../includes/sidebar.php"); ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <?php require_once(__DIR__ . "/../includes/navbar.php"); ?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">View Customer</h1>
          </div>
        <!-- UI GOES HERE-->
        <div class="card shadow mb-4">
            <div class="card-head shadow">
                <div class="row pl-2">
                    <div class="col-md-2">
                        <i class="fas fa-info-circle pt-3"></i>
                    </div>
                    <div class="col-md-10">
                        <p class="pt-3">Personal Details</p>
                    </div>
                    <!-- <p><i class="fas fa-info-circle"></i> Personal Details</p> -->
                </div>
            </div>
            <input type="hidden" name="customer_id" id="customer_id" value="<?=$id;?>">
            <div class="card-body">
                <div class="row mt-5">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2">
                                <h5>First Name</h5>
                            </div>
                            <div class="col-md-10">
                            <input  type="text" 
                                    name="first_name" 
                                    id="first_name" 
                                    class="form-control"
                                    value="Mihir" disabled/>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-3">
                                <h5>Last Name</h5>
                            </div>
                            <div class="col-md-9">
                            <input  type="text" 
                                    name="last_name" 
                                    id="last_name" 
                                    class="form-control"
                                    value="9511653350" disabled/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2">
                                <h5>Email</h5>
                            </div>
                            <div class="col-md-10">
                            <input  type="text" 
                                    name="email_id" 
                                    id="email_id" 
                                    class="form-control"
                                    value="mihir.chichria@gmail.com" disabled/>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-3">
                                <h5>Gender</h5>
                            </div>
                            <div class="col-md-9">
                            <input  type="text" 
                                    name="gender" 
                                    id="gender" 
                                    class="form-control"
                                    value="Male" disabled/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2">
                                <h5>GST No.</h5>
                            </div>
                            <div class="col-md-10">
                            <input  type="text" 
                                    name="gst_no" 
                                    id="gst_no" 
                                    class="form-control"
                                    value="29ABCDE1234F2Z5" disabled/>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-3">
                                <h5>Phone No.</h5>
                            </div>
                            <div class="col-md-9">
                            <input  type="text" 
                                    name="phone_no" 
                                    id="phone_no" 
                                    class="form-control"
                                    value="9511653350" disabled/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card shadow mt-5">
            <div class="card-head shadow">
                <div class="row pl-2">
                    <div class="col-md-2">
                    <i class="far fa-address-card pt-3"></i>
                    </div>
                    <div class="col-md-10">
                        <p class="pt-3">Address Details</p>
                    </div>
                    <!-- <p><i class="fas fa-info-circle"></i> Personal Details</p> -->
                </div>
            </div>
            <div class="card-body">
                <div class="row mt-5">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2">
                                <h5>Block No</h5>
                            </div>
                            <div class="col-md-10">
                                <input  type="text" 
                                        name="block_no" 
                                        id="block_no" 
                                        class="form-control"
                                        value="9511653350" disabled/>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2">
                                <h5>Street</h5>
                            </div>
                            <div class="col-md-10">
                                <input  type="text" 
                                        name="street" 
                                        id="street" 
                                        class="form-control"
                                        value="9511653350" disabled/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2">
                                <h5>City</h5>
                            </div>
                            <div class="col-md-10">
                                <input  type="text" 
                                        name="city" 
                                        id="city" 
                                        class="form-control"
                                        value="9511653350" disabled/>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2">
                                <h5>Pincode</h5>
                            </div>
                            <div class="col-md-10">
                                <input  type="text" 
                                        name="pincode" 
                                        id="pincode" 
                                        class="form-control"
                                        value="9511653350" disabled/>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row mt-4">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2">
                                <h5>State</h5>
                            </div>
                            <div class="col-md-10">
                                <input  type="text" 
                                        name="state" 
                                        id="state" 
                                        class="form-control"
                                        value="9511653350" disabled/>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2">
                                <h5>Country</h5>
                            </div>
                            <div class="col-md-10">
                                <input  type="text" 
                                        name="country" 
                                        id="country" 
                                        class="form-control"
                                        value="9511653350" disabled/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2">
                                <h5>Town</h5>
                            </div>
                            <div class="col-md-10">
                                <input  type="text" 
                                        name="town" 
                                        id="town" 
                                        class="form-control"
                                        value="9511653350" disabled/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once(__DIR__ . "/../includes/footer.php"); ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
  <?php
  require_once(__DIR__ . "/../includes/scroll-to-top.php");
  ?>
  <?php require_once(__DIR__ . "/../includes/core-scripts.php"); ?>
  <script src="<?=BASEASSETS;?>js/pages/customer/view-customer.js"></script>
</body>
</html>