<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
-
<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
  <div class="sidebar-brand-icon rotate-n-15">
    <i class="fas fa-laugh-wink"></i>
  </div>
  <div class="sidebar-brand-text mx-3">Quick ERP </div>
</a>

<!-- Divider -->
<hr class="sidebar-divider my-0">

<!-- Nav Item - Dashboard -->
<li class="nav-item <?= $sidebarSection == 'dashboard' ? 'active' : '';?>">
  <a class="nav-link" href="index.php">
    <i class="fas fa-fw fa-tachometer-alt"></i>
    <span>Dashboard</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
  Interface
</div>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item <?= $sidebarSection == 'category' ? 'active' : '';?>">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCategory" aria-expanded="true" aria-controls="collapseCategory">
    <i class="fas fa-fw fa-cog"></i>
    <span>Categories</span>
  </a>
  <div id="collapseCategory" class="collapse <?= $sidebarSection == 'category' ? 'show' : '';?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item <?= $sidebarSection == "category" & $sidebarSubSection == 'manage' ? 'active' : '';?>" href="manage-category.php">Manage Category</a>
      <a class="collapse-item <?= $sidebarSection == "category" & $sidebarSubSection == 'add' ? 'active' : '';?>" href="add-category.php">Add Category</a>
    </div>
  </div>
</li>


<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item <?= $sidebarSection == 'customer' ? 'active' : '';?>">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCustomer" aria-expanded="true" aria-controls="collapseCustomer">
    <i class="fas fa-fw fa-cog"></i>
    <span>Customers</span>
  </a>
  <div id="collapseCustomer" class="collapse <?= $sidebarSection == 'customer' ? 'show' : '';?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item <?= $sidebarSection == "customer" & $sidebarSubSection == 'manage' ? 'active' : '';?>"  href="manage-customer.php">Manage Customers</a>
      <a class="collapse-item <?= $sidebarSection == "customer" & $sidebarSubSection == 'add' ? 'active' : '';?>" href="add-customer.php">Add Customer</a>
    </div>
  </div>
</li>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item <?= $sidebarSection == 'employee' ? 'active' : '';?>">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseEmployee" aria-expanded="true" aria-controls="collapseEmployee">
    <i class="fas fa-fw fa-cog"></i>
    <span>Employees</span>
  </a>
  <div id="collapseEmployee" class="collapse <?= $sidebarSection == 'employee' ? 'show' : '';?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item <?= $sidebarSection == "employee" & $sidebarSubSection == 'manage' ? 'active' : '';?>"  href="manage-employee.com">Manage Employees</a>
      <a class="collapse-item <?= $sidebarSection == "employee" & $sidebarSubSection == 'add' ? 'active' : '';?>" href="add-employee.com">Add Employee</a>
    </div>
  </div>
</li>


<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item <?= $sidebarSection == 'products' ? 'active' : '';?>">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseProduct" aria-expanded="true" aria-controls="collapseProduct">
    <i class="fas fa-fw fa-cog"></i>
    <span>Products</span>
  </a>
  <div id="collapseProduct" class="collapse <?= $sidebarSection == 'products' ? 'show' : '';?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item <?= $sidebarSection == "products" & $sidebarSubSection == 'manage' ? 'active' : '';?>"  href="manage-product.php">Manage Products</a>
      <a class="collapse-item <?= $sidebarSection == "products" & $sidebarSubSection == 'check' ? 'active' : '';?>"  href="#">Check Current Inventory</a>
      <a class="collapse-item <?= $sidebarSection == "products" & $sidebarSubSection == 'add' ? 'active' : '';?>" href="add-product.php">Add Product</a>
    </div>
  </div>
</li>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item <?= $sidebarSection == 'supplier' ? 'active' : '';?>">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSupplier" aria-expanded="true" aria-controls="collapseSupplier">
    <i class="fas fa-fw fa-cog"></i>
    <span>Suppliers</span>
  </a>
  <div id="collapseSupplier" class="collapse <?= $sidebarSection == 'supplier' ? 'show' : '';?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item <?= $sidebarSection == "supplier" & $sidebarSubSection == 'manage' ? 'active' : '';?>"  href="#">Manage Suppliers</a>
      <a class="collapse-item <?= $sidebarSection == "supplier" & $sidebarSubSection == 'add' ? 'active' : '';?>" href="#">Add Supplier</a>
    </div>
  </div>
</li>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item <?= $sidebarSection == 'transaction' ? 'active' : '';?>">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTransaction" aria-expanded="true" aria-controls="collapseTransaction">
    <i class="fas fa-fw fa-cog"></i>
    <span>Transaction</span>
  </a>
  <div id="collapseTransaction" class="collapse <?= $sidebarSection == 'transaction' ? 'show' : '';?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item <?= $sidebarSection == "transaction" & $sidebarSubSection == 'purchase' ? 'active' : '';?>"  href="add-purchase.php">Purchase</a>
      <a class="collapse-item <?= $sidebarSection == "transaction" & $sidebarSubSection == 'sale' ? 'active' : '';?>" href="add-sale.php">Sale</a>
    </div>
  </div>
</li>



<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
  Addons
</div>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
    <i class="fas fa-fw fa-folder"></i>
    <span>Reports</span>
  </a>
  <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item " href="#">Purchase History</a>
      <a class="collapse-item" href="#">Sales Report</a>
      <a class="collapse-item" href="#">P &amp; L statement</a>
    </div>
  </div>
</li>

<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
  <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

</ul>
<!-- End of Sidebar -->