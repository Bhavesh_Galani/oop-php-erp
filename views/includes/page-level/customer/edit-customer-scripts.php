<script src="<?=BASEASSETS;?>vendor/jquery-validator/jquery.validate.min.js"></script>
<script src="<?=BASEASSETS;?>vendor/toastr/toastr.min.js"></script>

<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    <?php
    if(Session::hasSession(VALIDATION_ERROR)):
    ?>
        toastr.error("<?=Session::getSession(VALIDATION_ERROR);?>", "Failed!");

    <?php
        Session::unsetSession(VALIDATION_ERROR);
    ?>
    <?php
    elseif(Session::hasSession(EDIT_SUCCESS)):
    ?>
        toastr.success("<?=Session::getSession(EDIT_SUCCESS);?>", "Success");
    <?php
        Session::unsetSession(EDIT_SUCCESS);
    elseif(Session::hasSession(EDIT_ERROR)):
    ?>
        toastr.error("<?=Session::getSession(EDIT_ERROR);?>", "Failed!");
    <?php
        Session::unsetSession(EDIT_ERROR);
    ?>
    <?php
    endif;
    ?>
</script>
<script src="<?=BASEASSETS;?>js/pages/customer/edit-customer.js"></script>