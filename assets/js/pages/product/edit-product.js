$(function(){
    $("#edit-customer").validate({
        'rules': {
            'name':{
                required: true,
                minlength: 2,
                maxlength: 255
            },
            'specification':{
                required: true,
                minlength: 2,
                maxlength: 255
            },
            'hsn_code':{
                required: true,
                number: true,
                minlength: 8,
                maxlength: 8
            },
            'category_id': {
                required: true
            },
            'eoq_level': {
                required: true
            },
            'danger_level': {
                required: true
            },
            'quantity': {
                required: true
            },
            'selling_rate': {
                required: true
            }
        },
        submitHandler: function(form) {
            // do other things for a valid form
            form.submit();
        }
    });
    var baseURL = window.location.origin;
    var filePath = "/helper/routing.php";
    var id = $("#product_id").val();
    $.ajax({
        url: baseURL + filePath,
        method: "POST",
        data: {
            "product_id": id,
            "fetch": "product"
        },
        dataType: "json",
        success: function(data){
            for(var i in data[0]){
                if($("#"+i).val() == "")
                    $("#"+i).val(data[0][i]);
            }
            $('#category_id option[value="' + data[0]['category_id']+  '"]').attr("selected", "selected");
            var suppliers_id =  data[0]["supplier_id"].split(",");
            var suppliers = document.getElementById("supplier_id");
            for(var i in suppliers_id){
                $('#supplier_id option[value="' + suppliers_id[i]+  '"]').attr("selected", "selected");
            }
        }
    });
});