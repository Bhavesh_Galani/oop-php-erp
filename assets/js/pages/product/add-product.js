$(function(){
    $("#add-product").validate({
        'rules': {
            'name':{
                required: true,
                minlength: 2,
                maxlength: 255
            },
            'specification':{
                required: true,
                minlength: 2,
                maxlength: 255
            },
            'hsn_code':{
                required: true,
                number: true,
                minlength: 8,
                maxlength: 8
            },
            'category_id': {
                required: true,   
            },
            'eoq_level': {
                required: true
            },
            'danger_level': {
                required: true
            },
            'quantity': {
                required: true
            },
            'selling_rate': {
                required: true
            }
        },
        submitHandler: function(form) {
            // do other things for a valid form
            form.submit();
        }
    });
});