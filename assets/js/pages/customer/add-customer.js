$(function(){
    $("#add-customer").validate({
        'rules': {
            'first_name':{
                'required': true,
                'minlength': 2,
                'maxlength': 255
            },
            'last_name' : {
                'required' : true,
                'minlength' : 2,
                'maxlength' : 255
            },
            'gender': {
                'required': true
            },
            'phone_no' : {
                'required' : true,
                'minlength' : 10,
                'maxlength' : 10
            },
            'email_id' : {
                'required' : true,
                'email' : true
            },
            'country' : {
                'required' : true
            },
            'block_no' : {
                'required' : true,
                'minlength' : 3
            },
            'street' : {
                'required' : true,
                'minlength' : 3
            },
            'city' : {
                'required' : true,
                'minlength' : 3
            },
            'pincode' : {
                'required' : true,
                'minlength' : 6,
                'maxlength' : 6
            },
            'state' : {
                'required' : true,
                'minlength' : 2
            },
            'town': {
                'required': true,
                'minlength': 3
            }
        },
        submitHandler: function(form) {
            // do other things for a valid form
            form.submit();
        }
    });
});