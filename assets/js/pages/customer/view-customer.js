$(document).ready(function() {
    var baseURL = window.location.origin;
    var filePath = "/helper/routing.php";
    var id = $("#customer_id").val();
    // console.log(id);

    //For Customer
    $.ajax({
        url: baseURL + filePath,
        method: "POST",
        data: {
            "customer_id": id,
            "fetch": "customer"
        },
        dataType: "json",
        success: function(data){
            // $("#category_name").val(data[0].name);
            // console.log(data[0].first_name);
            for(var i in data[0]){
                // console.log(i + " => " + data[0][i]);
                $("#"+i).val(data[0][i]);
            }
        }
    });

    //For Address
    $.ajax({
        url: baseURL + filePath,
        method: "POST",
        data: {
            "customer_id": id,
            "fetch": "address"
        },
        dataType: "json",
        success: function(data){
            console.log(data);
            for(var i in data[0]){
                // console.log(i + " => ", data[0][i]);
                $("#"+i).val(data[0][i]);
            }
        }
    });
});