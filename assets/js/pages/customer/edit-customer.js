$(function(){
    $("#edit-customer").validate({
        'rules': {
            'first_name':{
                'required': true,
                'minlength': 2,
                'maxlength': 255
            },
            'last_name' : {
                'required' : true,
                'minlength' : 2,
                'maxlength' : 255
            },
            'gender': {
                'required': true
            },
            'phone_no' : {
                'required' : true,
                'minlength' : 10,
                'maxlength' : 10
            },
            'email_id' : {
                'required' : true,
                'email' : true
            },
            'country' : {
                'required' : true
            },
            'block_no' : {
                'required' : true,
                'minlength' : 3
            },
            'street' : {
                'required' : true,
                'minlength' : 3
            },
            'city' : {
                'required' : true,
                'minlength' : 3
            },
            'pincode' : {
                'required' : true,
                'minlength' : 6,
                'maxlength' : 6
            },
            'state' : {
                'required' : true,
                'minlength' : 2
            },
            'town': {
                'required': true,
                'minlength': 3
            }
        },
        submitHandler: function(form) {
            // do other things for a valid form
            form.submit();
        }
    });
    var baseURL = window.location.origin;
    var filePath = "/helper/routing.php";
    var id = $("#customer_id").val();
    // For Customer
    $.ajax({
        url: baseURL + filePath,
        method: "POST",
        data: {
            "customer_id": id,
            "fetch": "customer"
        },
        dataType: "json",
        success: function(data){
            // $("#category_name").val(data[0].name);
            // console.log(data[0].first_name);
            for(var i in data[0]){
                // console.log(i + " => ", data[0][i]);
                //If error has filled old data set in Session!
                if($("#"+i).val() == "")
                    $("#"+i).val(data[0][i]);
            }
            if(data[0]['gender'] == "Male"){
                document.getElementById("gender1").checked = true;
            }else{
                document.getElementById("gender1").checked = true;
            }
        }
    });
    //For Address
    $.ajax({
        url: baseURL + filePath,
        method: "POST",
        data: {
            "customer_id": id,
            "fetch": "address"
        },
        dataType: "json",
        success: function(data){
            for(var i in data[0]){
                // console.log(i + " => ", data[0][i]);
                //If error has filled old data set in Session!
                if($("#"+i).val() == "")
                    $("#"+i).val(data[0][i]);
            }
        }
    });
});