var TableDataTables = function(){
    var handleCustomerTable = function(){
        var manageCustomerTable = $("#manage-customer-datatable");
        var baseURL = window.location.origin;
        var filePath = "/helper/routing.php";
        var oTable = manageCustomerTable.dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "page": "manage_customer"
                }
            },
            "lengthMenu": [
                [5, 10, 20, -1],
                [5, 10, 20, "All"]
            ],
            "order": [
                [0, "ASC"]
            ],
            "columnDefs": [{
                'orderable': false,
                'targets': [-1]
            }]
        });

        manageCustomerTable.on('click', '.edit', function(){
            id = $(this).attr('id');
            $.ajax({
                url:baseURL + filePath,
                method: "POST",
                data: {
                    "customer_id": id,
                    "route_to": "edit-customer"
                },
                success: function(){
                    window.location = "edit-customer.php";
                }
            });
        });

        manageCustomerTable.on('click', '.view', function(){
            id = $(this).attr('id');
            $.ajax({
                url:baseURL + filePath,
                method: "POST",
                data: {
                    "customer_id": id,
                    "route_to": "view-customer"
                },
                success: function(){
                    window.location = "view-customer.php";
                }
            });
        });

        manageCustomerTable.on('click', '.delete', function(){
            id = $(this).attr('id');
            $("#record_id").val(id);
        });
    }
    return{
        init: function(){
            handleCustomerTable();
        }
    }
}();
jQuery(document).ready(function(){
    TableDataTables.init();
});