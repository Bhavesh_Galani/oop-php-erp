/**
 * First when ajax was not there programmers used to store the number of rows which were added in list in the SESSIONS. But for more security, they used to store in database and reload the page because XHR/ajax was not there.
 * So in our example when we select category they may reload the page and then add the products that the category belongs.
 */
var id = 2;
var baseURL = window.location.origin;
var filePath = '/helper/routing.php';
var totalAmount = 0;
var isCustomerVerified = false;
var customerID = -1;

/*********************************
 ************FUNCTIONS************
 ********************************/

function deleteProduct(delete_id){
    var elements = document.getElementsByClassName("product_row");
    if(elements.length != 1){
        updateFileTotal("-" + $("#final_rate_" + delete_id).val());
        $("#element_"+delete_id).remove();
    }
}

function addProduct(){
    $("#products_container").append(
        `<!-- BEGIN: PRDOUCT CUSTOM CONTROL -->
        <div class="row product_row" id="element_${id}">
            <!-- BEGIN: CATEGORY SELECT -->
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">Category</label>
                    <select name="" id="category_${id}" class="form-control category_select">
                        <option disabled selected>Select Category</option>
                    </select>
                </div>
            </div>
            <!-- END: CATEGORY SELECT -->

            <!-- BEGIN: PRODUCTS SELECT -->
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Products</label>
                    <select name="product_id[]" id="product_${id}" class="form-control product_select">
                        <option disabled selected>Select Product</option>
                    </select>
                </div>
            </div>
            <!-- END: PRODUCTS SELECT -->

            <!-- BEGIN: SELLING PRICE -->
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">Selling Price</label>
                    <input type="text" id="selling_price_${id}" class="form-control" disabled>
                </div>
            </div>
            <!-- END: SELLING PRICE -->

            <!-- BEGIN: QUANTITY -->
            <div class="col-md-1">
                <div class="form-group">
                    <label for="">Quantity</label>
                    <input type="number" name="quantity[]" id="quantity_${id}" class="form-control quantity_input" value='0' min="0">
                </div>
            </div>
            <!-- END: QUANTITY -->

            <!-- BEGIN: DISCOUNT -->
            <div class="col-md-1">
                <div class="form-group">
                    <label for="">Discount</label>
                    <input type="number" name="discount[]" id="discount_${id}" class="form-control discount_input" value='0' min="0">
                </div>
            </div>
            <!-- END: DISCOUNT -->

            <!--BEGIN: FINAL RATE-->
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">Final Rate</label>
                    <input type="number" id="final_rate_${id}" class="form-control final_rate_value" value="0" readonly min="0">
                </div>
            </div>
            <!--END: FINAL RATE-->

            <!-- BEGIN: DELETE BUTTON -->
            <div class="col-md-1">
                <button onclick="deleteProduct(${id})" type="button" class="btn btn-danger" style="margin-top:40%"><i class="far fa-trash-alt"></i></button>
            </div>
            <!-- END: DISCOUNT -->
        </div>`
    );

    $.ajax({
        url: baseURL + filePath,
        method: 'POST',
        data:{
            getCategories: true
        },
        dataType: 'json',
        success: function(categories){
            categories.forEach(function(category){
                $("#category_"+id).append(
                    `<option value='${category.id}'>${category.name}</option>`
                );
            });
            id++;
        }
    });
}
function calculateFinalRate(element_id){
    var selling_price   =   $("#selling_price_"+element_id).val();
    var quantity        =   $("#quantity_"+element_id).val();
    if((selling_price != "")){
        var final_rate = selling_price*quantity;

        var discount = $("#discount_"+element_id).val();
        if(discount != 0){
            final_rate = final_rate - (final_rate * discount/100);
        }
        $("#final_rate_"+element_id).val(final_rate);
        
    }
}
function updateFileTotal(value){
    $("#finalTotal").val(parseInt($("#finalTotal").val()) + parseInt(value));
}
function clearAll(element_id){
    $("#quantity_"+element_id).val(0);
    $("#discount_"+element_id).val(0);
    updateFileTotal("-" + $("#final_rate_"+element_id).val());
    $("#final_rate_"+element_id).val(0);
    $("#selling_price_"+element_id).val("");
}
/*********************************
 **************EVENTS*************
 ********************************/

$("#products_container").on('change', '.category_select', function(){
    var element_id = $(this).attr("id").split('_')[1];
    var category_id = this.value;
    $.ajax({
        url: baseURL+filePath,
        method: "POST",
        data: {
            getProductsByCategoryID: true,
            categoryID: category_id
        },
        dataType: 'json',
        success: function(products){
            clearAll(element_id);
            $("#product_"+element_id).empty();
            $("#product_"+element_id).append(`<option disabled selected>Select Category</option>`);
            products.forEach(function(product){
                $("#product_"+element_id).append(
                    `<option value='${product.id}'>${product.name}</option>`
                );
            });
        }
    });
});
$("#products_container").on('change', '.product_select', function(){
    var element_id = $(this).attr("id").split('_')[1];
    var product_id = this.value;
    $.ajax({
        url: baseURL+filePath,
        method: "POST",
        data: {
            getSellingPriceOfProduct: true,
            productID: product_id
        },
        dataType: 'text',
        success: function(price){
            clearAll(element_id);
            $("#selling_price_"+element_id).val(price);
        }
    });
});
$("#products_container").on('input', '.quantity_input, .discount_input', function(){
    if($(this).val() < "0"){
        $(this).val("0");
    }else{
        $(this).val(1*$(this).val());
    }
    var element_id = $(this).attr("id").split('_')[1];
    var oldValue = $("#final_rate_" + element_id).val();
    console.log(oldValue);
    calculateFinalRate(element_id);
    updateFileTotal($("#final_rate_" + element_id).val()-oldValue);
});
var error = false;
const re = /^\w([.-]?\w+)*@\w+(-?\w+)*(\.\w+)$/;
$("#customer_email").on('input', function(){
    var email = $("#customer_email");
    if(re.test(email.val()) && error){

        error = false;
        $("#email-error").remove();
        email.removeClass("error");
    }
});
$("#check_email").click(function(){
    var email = $("#customer_email");
    if(!re.test(email.val()) && !error){
        error = true;
        email.after(`<label id="email-error" class="error" for="danger_level">Enter a valid Email</label>`);
        email.addClass("error");
    }else{
        $.ajax({
            url: baseURL+filePath,
            method: "POST",
            data: {
                getCustomerByEmail: true,
                email: $("#customer_email").val()
            },
            dataType: 'text',
            success: function(customer_id){
                if(customer_id == ""){
                    customerID = -1;
                    isCustomerVerified = false;
                    $("#email_verify_failed").css("display", "inline-block");
                    $("#add_customer_btn").css("display", "inline-block");
                }else{
                    customerID = customer_id;
                    isCustomerVerified = true;
                    document.getElementById("customer_email").disabled = true;
                    $("#customer_id").val(customer_id);
                    $("#email_verify_success").css("display", "block");
                    $("#email_verify_failed").css("display", "none");
                    $("#add_customer_btn").css("display", "none");
                    $("#check_email").css("display", "none");
                }
            }
        });
    }    
});

// Form Submit
$("#sales_form").submit(function(event){
    console.log(isCustomerVerified);
    if((!isCustomerVerified) ||  $("#customer_id").val() != customerID){
        if(!error){
            $("#customer_email").after(`<label id="email-error" class="error" for="danger_level">Enter a valid Email</label>`);
            $("#customer_email").addClass("error");
            error = true;
        }
        if($("#customer_id").val() != customerID){
            document.getElementById("customer_email").disabled = false;
            $("#email_verify_success").css("display", "none");
            $("#email_verify_failed").css("display", "inline-block");
            $("#add_customer_btn").css("display", "inline-block");
        }
        event.preventDefault();
        return;
    }
    $('.    ').each(function(){
        var element_id = $(this).attr("id").split('_')[1];
        if ($(this).val() == 0 || $("#product_" + element_id).val() == null) {
            $("#element_" + element_id).remove();
        }
    });
  });