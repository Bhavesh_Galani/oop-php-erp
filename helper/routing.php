<?php
require_once 'init.php';

/****************************************************************
 ****************************CANTEGORY MANAGEMENT******************
 ****************************************************************/

if(isset($_POST['add_category']))
{
    //USER HAS REQUESTED TO ADD A NEW CATEGORY
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('category')->addCategory($_POST);
        switch($result)
        {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR, 'There was problem while inserting record, please try again later!');
                Util::redirect('manage-category.php');
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS, 'The record have been added successfully!');
                // Util::dd();
                Util::redirect('manage-category.php');
                break;
            case VALIDATION_ERROR:
                Session::setSession(VALIDATION_ERROR,'There was some problem in validating your data at service side');
                Session::setSession('errors',serialize($di->get('validator')->errors()));
                Session::setSession('old', $_POST);
                Util::redirect('add-category.php');
                break;
        }
    }
}

if(isset($_POST['edit_category']))
{
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('category')->update($_POST,$_POST['category_id']);
        // Util::dd($_POST);
        switch($result)
        {
            case EDIT_ERROR:
                Session::setSession(EDIT_ERROR, 'There was problem while editing record, please try again later!');
                Util::redirect('manage-category.php');
                break;
            case EDIT_SUCCESS:
                Session::setSession(EDIT_SUCCESS, 'The record have been edited successfully!');
                // Util::dd();
                Util::redirect('manage-category.php');
                break;
            case VALIDATION_ERROR:
                Session::setSession(VALIDATION_ERROR,'There was some problem in validating your data at service side');
                Session::setSession('errors',serialize($di->get('validator')->errors()));
                Session::setSession('old', $_POST);
                Util::redirect('manage-category.php');
                break;
        }
    }
}

if(isset($_POST['delete_category']))
{
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('category')->delete($_POST['record_id']);
        switch($result)
        {
            case DELETE_ERROR:
                Session::setSession(DELETE_ERROR, 'There was problem while deleting record, please try again later!');
                Util::redirect('manage-category.php');
                break;
            case DELETE_SUCCESS:
                Session::setSession(DELETE_SUCCESS, 'The record have been deleted successfully!');
                // Util::dd();
                Util::redirect('manage-category.php');
                break;
        }
    }
}

/****************************************************************
 ****************************CUSTOMER MANAGEMENT******************
 ****************************************************************/


if(isset($_POST['add_customer']))
{
    //USER HAS REQUESTED TO ADD A NEW CUSTOMER
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        // Util::dd($_POST);
        $result = $di->get('customer')->addCustomer($_POST);
        switch($result)
        {
            case ADD_ERROR:
                // Util::dd("Add_Error");
                Session::setSession(ADD_ERROR, 'There was problem while inserting record, please try again later!');
                Util::redirect('manage-customer.php');
                break;
            case ADD_SUCCESS:
                // Util::dd("Add_Success");
                Session::setSession(ADD_SUCCESS, 'The record have been added successfully!');
                // Util::dd();
                Util::redirect('manage-customer.php');
                break;
            case VALIDATION_ERROR:
                // Util::dd("Add_Error");
                Session::setSession('errors',serialize($di->get('validator')->errors()));
                Session::setSession('old', $_POST);
                Util::redirect('add-customer.php');
                break;
        }
    }
}

if(isset($_POST['edit_customer']))
{
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        // Util::dd(Session::getSession("edit-customer-id"));
        // Util::dd($_POST['customer_id']);
        //This is to ensure that customer_id is not changed by user by inspecting the code!
        if(Session::getSession("edit-customer-id") == $_POST['customer_id']){
            $result = $di->get('customer')->update($_POST,$_POST['customer_id']);
            switch($result)
            {
                case EDIT_ERROR:
                    Session::setSession(EDIT_ERROR, 'There was problem while editing record, please try again later!');
                    Util::redirect('edit-customer.php');
                    break;
                case EDIT_SUCCESS:
                    Session::setSession(EDIT_SUCCESS, 'The record have been edited successfully!');
                    // Util::dd();
                    Util::redirect('manage-customer.php');
                    break;
                case VALIDATION_ERROR:
                    Session::setSession(VALIDATION_ERROR,'There was some problem in validating your data at service side');
                    Session::setSession('errors',serialize($di->get('validator')->errors()));
                    Session::setSession('old', $_POST);
                    Util::redirect('edit-customer.php');
                    break;
            }
        }else{
            Util::redirect("masti.php");
        }
    }
}

if(isset($_POST['delete_customer']))
{
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('customer')->delete($_POST['record_id']);
        switch($result)
        {
            case DELETE_ERROR:
                Session::setSession(DELETE_ERROR, 'There was problem while deleting record, please try again later!');
                Util::redirect('manage-customer.php');
                break;
            case DELETE_SUCCESS:
                Session::setSession(DELETE_SUCCESS, 'The record have been deleted successfully!');
                // Util::dd();
                Util::redirect('manage-customer.php');
                break;
        }
    }
}

/****************************************************************
 ****************************PRODUCT MANAGEMENT******************
 ****************************************************************/

if(isset($_POST['add_product']))
{
    //USER HAS REQUESTED TO ADD A NEW CATEGORY
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('product')->addProduct($_POST);
        switch($result)
        {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR, 'There was problem while inserting record, please try again later!');
                Util::redirect('manage-product.php');
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS, 'The record have been added successfully!');
                // Util::dd();
                Util::redirect('manage-product.php');
                break;
            case VALIDATION_ERROR:
                Session::setSession(VALIDATION_ERROR, 'There was some problem in validating your data at server side!');
                Session::setSession('errors', serialize($di->get('validator')->errors()));
                Session::setSession('old', $_POST);
                Util::redirect('add-product.php');
                break;
        }
    }
}
if(isset($_POST['edit_product']))
{
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        //This is to ensure that product_id is not changed by user by inspecting the code!
        if(Session::getSession("edit-product-id") == $_POST['product_id']){
            $result = $di->get('product')->update($_POST,$_POST['product_id']);
            switch($result)
            {
                case EDIT_ERROR:
                    Session::setSession(EDIT_ERROR, 'There was problem while editing record, please try again later!');
                    Util::redirect('edit-product.php');
                    break;
                case EDIT_SUCCESS:
                    Session::setSession(EDIT_SUCCESS, 'The record have been edited successfully!');
                    // Util::dd();
                    Util::redirect('manage-product.php');
                    break;
                case VALIDATION_ERROR:
                    Session::setSession(VALIDATION_ERROR,'There was some problem in validating your data at service side');
                    Session::setSession('errors',serialize($di->get('validator')->errors()));
                    Session::setSession('old', $_POST);
                    Util::redirect('edit-product.php');
                    break;
            }
        }else{
            Util::redirect("masti.php");
        }
    }
}

/****************************************************************
 ****************************SALES MANAGEMENT********************
 ****************************************************************/
if(isset($_POST["add_sales"])){
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST)){
        $result = $di->get('sales')->create($_POST);
        switch($result)
        {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR, 'There was problem while inserting record, please try again later!');
                Util::redirect('add-sale.php');
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS, 'The record have been added successfully!');
                $inv_id = $_SESSION['inv_id'];
                unset($_SESSION['inv_id']);
                Util::redirect('invoice.php?inv_id='.$inv_id);
                break;
            case VALIDATION_ERROR:
                Session::setSession(VALIDATION_ERROR, 'There was some problem in validating your data at server side!');
                Session::setSession('errors', serialize($di->get('validator')->errors()));
                Session::setSession('old', $_POST);
                Util::redirect('add-sale.php');
                break;
        }

    }
}

/****************************************************************
 **************************** MANAGING DATATABLES ***************
 ****************************************************************/


if(isset($_POST['page']) && $_POST['page'] == 'manage_customer')
{
    // Util::dd($_POST);
    $search_parameter = $_POST['search']['value'] ?? null;
    $order_by = $_POST['order'] ?? null;
    $start = $_POST['start'];
    $length = $_POST['length'];
    $draw = $_POST['draw'];
    $di->get("customer")->getJSONDataForDataTable($draw,$search_parameter,$order_by,$start,$length);
}

if(isset($_POST['page']) && $_POST['page'] == 'manage_product')
{
    // Util::dd($_POST);
    $search_parameter = $_POST['search']['value'] ?? null;
    $order_by = $_POST['order'] ?? null;
    $start = $_POST['start'];
    $length = $_POST['length'];
    $draw = $_POST['draw'];
    $di->get("product")->getJSONDataForDataTable($draw,$search_parameter,$order_by,$start,$length);
}
if(isset($_POST['page']) && $_POST['page'] == 'manage_category')
{
    $search_parameter = $_POST['search']['value'] ?? null;
    $order_by = $_POST['order'] ?? null;
    $start = $_POST['start'];
    $length = $_POST['length'];
    $draw = $_POST['draw'];
    $di->get("category")->getJSONDataForDataTable($draw,$search_parameter,$order_by,$start,$length);
}


/****************************************************************
 ****************************FETCHING ***************************
 ****************************************************************/

if(isset($_POST['fetch']) && $_POST['fetch'] == 'category')
{
    $category_id = $_POST['category_id'];
    $result = $di->get('category')->getCategoryById($category_id,PDO::FETCH_ASSOC);
    echo json_encode($result);
}
if(isset($_POST['fetch']) && $_POST['fetch'] == 'product')
{
    $product_id = $_POST['product_id'];
    $result = $di->get('product')->getProductById($product_id,PDO::FETCH_ASSOC);
    echo json_encode($result);
}

if(isset($_POST['fetch']) && $_POST['fetch'] == 'customer')
{
    $customer_id = $_POST['customer_id'];
    $result = $di->get('customer')->getCustomerById($customer_id,PDO::FETCH_ASSOC);
    echo json_encode($result);
}

if(isset($_POST['fetch']) && $_POST['fetch'] == 'address')
{
    $customer_id = $_POST['customer_id'];
    $result = $di->get('customer')->getAddressById($customer_id, PDO::FETCH_ASSOC);
    // Util::dd($result);
    echo json_encode($result);

}

/****************************************************************
 ****************************ROUTING*****************************
 ****************************************************************/

if(isset($_POST['route_to'])){
    if($_POST['route_to'] == 'view-customer'){
        Session::setSession("view-customer-id", $_POST['customer_id']);
    }

    if($_POST['route_to'] == "edit-customer"){
        Session::setSession("edit-customer-id", $_POST['customer_id']);
    }

    if($_POST['route_to'] == "edit-product"){
        Session::setSession("edit-product-id", $_POST['product_id']);
    }
}

//Ajax Routings to Fetch Data
if(isset($_POST['getCategories'])){
    echo json_encode($di->get('category')->all());
}
if(isset($_POST['getProductsByCategoryID'])){
    $category_id = $_POST['categoryID'];
    echo json_encode($di->get('product')->getProductsByCategoryID($category_id));
}
if(isset($_POST['getSellingPriceOfProduct'])){
    echo ($di->get('product')->getSellingPrice($_POST['productID']));
}
if(isset($_POST['getCustomerByEmail'])){
    echo ($di->get('customer')->getCustomerByEmail($_POST['email']));
}
?>